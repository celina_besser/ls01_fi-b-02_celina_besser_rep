
public class Fakultaet {

	public static void main(String[] args) {
		System.out.printf("%-5s"+"%-20s"+"%-4s"+"%d%n","0!","=","=",1);
		System.out.printf("%-5s"+"%-20s"+"%-4s"+"%d%n","1!","= 1","=",1);
		System.out.printf("%-5s"+"%-20s"+"%-4s"+"%d%n","1!","= 1 * 2","=",2);
		System.out.printf("%-5s"+"%-20s"+"%-4s"+"%d%n","1!","= 1 * 2 * 3","=",(2*3));
		System.out.printf("%-5s"+"%-20s"+"%-4s"+"%d%n","1!","= 1 * 2 * 3 * 4","=",(6*4));
		System.out.printf("%-5s"+"%-20s"+"%-4s"+"%d%n","1!","= 1 * 2 * 3 * 4 * 5","=",(5*24));

	}

}
