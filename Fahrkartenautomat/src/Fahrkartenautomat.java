﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       boolean stop = false;
       double zuZahlenderBetrag; 
       double rückgabebetrag;
	   
       do {
    	   // Bestellung erfassen
    	   zuZahlenderBetrag = fahrkartenbestellungErfassen();
       
    	   // Geldeinwurf
    	   rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       
    	   // Fahrscheinausgabe
    	   fahrkartenAusgeben();
       
    	   // Rückgeldberechnung und -Ausgabe
    	   rueckgeldAusgeben(rückgabebetrag);
       
    	   System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
    			   "vor Fahrtantritt entwerten zu lassen!\n"+
    			   "Wir wünschen Ihnen eine gute Fahrt.\n");
       } while (!stop);
    }
    
    // Bestellung erfassen
    // -------------------
    static double fahrkartenbestellungErfassen() {
    	double einzelpreis = 0;
    	double zuZahlenderBetrag = 0;
    	int fahrkartenAuswahl;
    	int tickets;
    	int bezahlen = 11; //eine Variable für die Eingabe wenn der Nutzer bezahlen will
    	Scanner tastatur = new Scanner(System.in);
    	String[] fahrkartenBezeichnung = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC",
				"Einzelfahrschein Berlin ABC", "Kurzstrecke",
				"Tageskarte Berlin AB", "Tageskarte Berlin BC",
				"Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB",
				"Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
    	double[] fahrkartenPreis = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
    	
    	do {
	    	do {
	    		System.out.print("\nWählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n");
	    		//Angeben der verschiedenen Fahrkarten mithilfe des Arrays
	    			for (int auswahl = 0; auswahl < fahrkartenBezeichnung.length; auswahl++) {
	    				System.out.print("  " + fahrkartenBezeichnung[auswahl] + " (" + (auswahl+1) +")\n");
	    			}
	    		System.out.print("  Bezahlen " + "(" + bezahlen + ")\n\n");

	    		//Der Nutzer gibt seine Wahl ein
	    		System.out.print("Ihre Wahl: ");
	    		fahrkartenAuswahl = tastatur.nextInt();
	    		
	    		//Der Einzelpreis wird mithilfe des Arrays ermittelt
	    		if (fahrkartenAuswahl <= fahrkartenBezeichnung.length && fahrkartenAuswahl > 0) {
	    			einzelpreis = fahrkartenPreis[fahrkartenAuswahl-1];
	    		}
	    		//Wenn der Nutzer bezahlen will, wird die Funktion beendet
	    		else if (fahrkartenAuswahl == bezahlen && zuZahlenderBetrag > 0) {
	    			return zuZahlenderBetrag;
	    		}
	    		else {
	    			System.out.println(" >>falsche Eingabe<<\n");
	    		}
	    	} while (fahrkartenAuswahl > fahrkartenPreis.length || fahrkartenAuswahl <= 0
	    			|| fahrkartenAuswahl == bezahlen && zuZahlenderBetrag == 0);
	       
	 	   do {
	 		   System.out.print("Anzahl der Tickets: ");
	  	   
	 		   tickets = tastatur.nextInt();
	 		   if (tickets <= 10 && tickets >= 1) {
	 			   zuZahlenderBetrag += einzelpreis*tickets;
	 		   }
	 		   
	 		   if (tickets > 10 || tickets < 1) {
	 			   System.out.println("Bitte geben Sie eine Ticketanzahl von 1 bis 10 ein!\n");
	 		   }
	 	   }while (tickets > 10 || tickets < 1);
    	}while (fahrkartenAuswahl <= fahrkartenBezeichnung.length);

 	   return zuZahlenderBetrag;
    }
    
    // Geldeinwurf
    // ------------------
    static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	double eingezahlterGesamtbetrag;
    	double eingeworfeneMünze;
        double rückgabebetrag;
        Scanner tastatur = new Scanner(System.in);
        
    	eingezahlterGesamtbetrag = 0.0;
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
    	{
    		System.out.printf("Noch zu zahlen: %.2f%n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    		System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    		eingeworfeneMünze = tastatur.nextDouble();
    		eingezahlterGesamtbetrag += eingeworfeneMünze;
    	}
    	rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    	return rückgabebetrag;
	}
    
    // Fahrscheinausgabe
    // ---------------------
    static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    // Rückgeldberechnung und -Ausgabe
    // ---------------------------------
    static void rueckgeldAusgeben(double rückgabebetrag) {
        if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f %s%n", rückgabebetrag, "EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }
    }
}