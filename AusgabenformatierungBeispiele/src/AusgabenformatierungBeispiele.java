
public class AusgabenformatierungBeispiele {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Hi");
		System.out.printf("|%-20.3s|%n","123456789");
		System.out.printf("|%+-20d|%n",123456789);
		System.out.printf("|%+020d|%n",123456789);
		System.out.printf("|%-20.2f|%n",123456.789);
		
		System.out.printf("%5s%n","**");
		System.out.printf("%-7s","*");
		System.out.print("*\n");
		System.out.printf("%-7s","*");
		System.out.print("*\n");
		System.out.printf("%5s%n","**");
		
		System.out.printf("%-5s"+"%-20s"+"%-4s"+"%d%n","0!","=","=",1);
		System.out.printf("%-5s"+"%-20s"+"%-4s"+"%d%n","1!","= 1","=",1);
		System.out.printf("%-5s"+"%-20s"+"%-4s"+"%d%n","1!","= 1 * 2","=",2);
		System.out.printf("%-5s"+"%-20s"+"%-4s"+"%d%n","1!","= 1 * 2 * 3","=",(2*3));
		System.out.printf("%-5s"+"%-20s"+"%-4s"+"%d%n","1!","= 1 * 2 * 3 * 4","=",(6*4));
		System.out.printf("%-5s"+"%-20s"+"%-4s"+"%d%n","1!","= 1 * 2 * 3 * 4 * 5","=",(5*24));
		
		System.out.printf("%-12s|"+"%10s%n", "Fahrenheit","Celsius");
		System.out.printf("-----------------------%n");
		System.out.printf("%-12d|"+"%10.2f%n", -20,-28.8889);
		System.out.printf("%-12d|"+"%10.2f%n", -10,-23.3333);
		System.out.printf("%-12d|"+"%10.2f%n", 0,-17.7778);
		System.out.printf("%-12d|"+"%10.2f%n", 20,-6.6667);
		System.out.printf("%-12d|"+"%10.2f%n", 30,-1.1111);

		/*System.out.printf("%5d!",1);
		System.out.printf("%5d!",2);
		System.out.printf("%5d!",3);
		System.out.printf("%5d!",4);
		System.out.printf("%5d!",5); */

	}

}
